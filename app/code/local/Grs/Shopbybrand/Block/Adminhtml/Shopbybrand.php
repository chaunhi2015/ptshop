<?php
class Grs_Shopbybrand_Block_Adminhtml_Shopbybrand extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_shopbybrand';
    $this->_blockGroup = 'shopbybrand';
    $this->_headerText = Mage::helper('shopbybrand')->__('Item Manager');
    $this->_addButtonLabel = Mage::helper('shopbybrand')->__('Add Item');
    parent::__construct();
  }
}
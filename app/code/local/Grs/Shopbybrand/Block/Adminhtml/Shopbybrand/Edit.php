<?php

class Grs_Shopbybrand_Block_Adminhtml_Shopbybrand_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'shopbybrand';
        $this->_controller = 'adminhtml_shopbybrand';
        
        $this->_updateButton('save', 'label', Mage::helper('shopbybrand')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('shopbybrand')->__('Delete Item'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('shopbybrand_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'shopbybrand_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'shopbybrand_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('shopbybrand_data') && Mage::registry('shopbybrand_data')->getId() ) {
            return Mage::helper('shopbybrand')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('shopbybrand_data')->getTitle()));
        } else {
            return Mage::helper('shopbybrand')->__('Add Item');
        }
    }
}
<?php

class Grs_Shopbybrand_Block_Adminhtml_Shopbybrand_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('shopbybrand_form', array('legend'=>Mage::helper('shopbybrand')->__('Item information')));
     
	  $object = Mage::getModel('shopbybrand/shopbybrand')->load( $this->getRequest()->getParam('id') );
	  $imgPath = Mage::getBaseUrl('media')."Brands/images/thumb/".$object['filename'];
	  
	  /*
      $fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('shopbybrand')->__('Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title',
      ));
	  */
	   $fieldset->addField('title', 'select', array(
          'label'     => Mage::helper('shopbybrand')->__('Brand Name'),
		  'name'      => 'title',
          'values'      => Mage::helper('shopbybrand')->getAttributes('manufacturer'), 
      ));

      $fieldset->addField('filename', 'file', array(
          'label'     => Mage::helper('shopbybrand')->__('File'),
          'required'  => false,
          'name'      => 'filename',
	  ));
	  //thumb
	   if( $object->getId() ){
		  $tempArray = array(
				  'name'      => 'filethumbnail',
				  'style'     => 'display:none;',
			  );
		  $fieldset->addField($imgPath, 'thumbnail',$tempArray);
		  //brandname
		 
	  }
	   
		
      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('shopbybrand')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('shopbybrand')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('shopbybrand')->__('Disabled'),
              ),
          ),
      ));
     
      $fieldset->addField('content', 'editor', array(
          'name'      => 'content',
          'label'     => Mage::helper('shopbybrand')->__('Content'),
          'title'     => Mage::helper('shopbybrand')->__('Content'),
          'style'     => 'width:500px; height:200px;',
          'wysiwyg'   => false,
          'required'  => true,
      ));
     
      if ( Mage::getSingleton('adminhtml/session')->getShopbybrandData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getShopbybrandData());
          Mage::getSingleton('adminhtml/session')->setShopbybrandData(null);
      } elseif ( Mage::registry('shopbybrand_data') ) {
          $form->setValues(Mage::registry('shopbybrand_data')->getData());
      }
      return parent::_prepareForm();
  }
}
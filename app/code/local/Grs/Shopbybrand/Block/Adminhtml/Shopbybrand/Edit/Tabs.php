<?php

class Grs_Shopbybrand_Block_Adminhtml_Shopbybrand_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('shopbybrand_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('shopbybrand')->__('Item Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('shopbybrand')->__('Item Information'),
          'title'     => Mage::helper('shopbybrand')->__('Item Information'),
          'content'   => $this->getLayout()->createBlock('shopbybrand/adminhtml_shopbybrand_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}
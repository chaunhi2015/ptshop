<?php

class Grs_Shopbybrand_Block_Adminhtml_Shopbybrand_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('shopbybrandGrid');
      $this->setDefaultSort('shopbybrand_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('shopbybrand/shopbybrand')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('shopbybrand_id', array(
          'header'    => Mage::helper('shopbybrand')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'shopbybrand_id',
      ));
	$this->addColumn('thumb', array(
          'header'    => Mage::helper('banners')->__('Thumbnail'),
          'align'     =>'center',
          'index'     => 'thumb',
		  'type'      => 'text',
		  'width'     => '150px',
      ));
	   $this->addColumn('brandname', array(
          'header'    => Mage::helper('shopbybrand')->__('Title'),
          'align'     =>'left',
          'index'     => 'brandname',
      ));
	  /*
      $this->addColumn('title', array(
          'header'    => Mage::helper('shopbybrand')->__('Title'),
          'align'     =>'left',
          'index'     => 'title',
      ));
		*/
		
	  /*
      $this->addColumn('content', array(
			'header'    => Mage::helper('shopbybrand')->__('Item Content'),
			'width'     => '150px',
			'index'     => 'content',
      ));
	  */

      $this->addColumn('status', array(
          'header'    => Mage::helper('shopbybrand')->__('Status'),
          'align'     => 'left',
          'width'     => '80px',
          'index'     => 'status',
          'type'      => 'options',
          'options'   => array(
              1 => 'Enabled',
              2 => 'Disabled',
          ),
      ));
	  
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('shopbybrand')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('shopbybrand')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('shopbybrand')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('shopbybrand')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('shopbybrand_id');
        $this->getMassactionBlock()->setFormFieldName('shopbybrand');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('shopbybrand')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('shopbybrand')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('shopbybrand/status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('shopbybrand')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('shopbybrand')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}
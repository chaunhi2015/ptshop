<?php
class Grs_Shopbybrand_Block_Shopbybrand extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getShopbybrand()     
     { 
        if (!$this->hasData('shopbybrand')) {
            $this->setData('shopbybrand', Mage::registry('shopbybrand'));
        }
        return $this->getData('shopbybrand');
        
    }
}
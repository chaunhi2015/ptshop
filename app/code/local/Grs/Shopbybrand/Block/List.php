<?php
class GRS_Shopbybrand_Block_List extends Mage_Catalog_Block_Product_List
{
  
	
	public function _getProductCollection($numberProduct =16)  
	{
		return $this->getProductsbyAtributte();
	}
	public function getManufacturer()
	{
		$brandname=$this->getRequest()->getParam('id');
		$brandname=str_replace("_"," ",$brandname);
		return $brandname;
	}
	public function getProductsbyAtributte()
	{
		
			//
			$storeId = Mage::app()->getStore()->getId();
			$brandname=$this->getRequest()->getParam('id');
			$brandname=str_replace("_"," ",$brandname);
			
			$attrName=$this->GetAttributeValue($brandname); // the brand name
			//$this->debug($attrName);
			$page = $this->getRequest()->getParam('p'); 
			//
			$collection = Mage::getModel('catalog/product')->setStoreId($storeId)->getCollection();
			$collection->addAttributeToFilter('manufacturer', array('in' => array($attrName)));
			$collection->addAttributeToSelect('*');
			$collection->addStoreFilter($storeId);
			$collection->getSize();
			// defaulrt product on page
			$defaultProduct=$this->getDefaultPerPageValue();
			if(!isset($defaultProduct)) $defaultProduct=16;
			// pager			
			if(isset($page)) 
			{
				if( $page >1 ) 
					$collection->getSelect()->limit($defaultProduct, ($page - 1) * $defaultProduct); 
				else 
					$collection->getSelect()->limit($defaultProduct);		
			}
			else
			{
				$collection->getSelect()->limit($defaultProduct); 
			}
			$collection->load();
			$this->_productCollection = $collection;
		
			return $this->_productCollection ;
	}
	public function GetAttributeValue($attributeLable)
	{
		$attributes = Mage::getModel('catalogsearch/advanced')->getAttributes();
		$attributeArray=array();
		foreach($attributes as $a){
		if($a->getAttributeCode() == 'manufacturer')
		{
			foreach($a->getSource()->getAllOptions(false) as $option){
			
			if ( mb_strtolower($option['label'],'UTF-8')==mb_strtolower($attributeLable,'UTF-8'))
				{
					return $option['value'];
				}
			}
			
		}
		}
		return "";

	}
	public function GetAttributeLabel($attributeValue)
	{
		$attributes = Mage::getModel('catalogsearch/advanced')->getAttributes();
		$attributeArray=array();
		foreach($attributes as $a){
		if($a->getAttributeCode() == 'manufacturer')
		{
			foreach($a->getSource()->getAllOptions(false) as $option){
			
			if ( mb_strtolower($option['value'],'UTF-8')==mb_strtolower($attributeValue,'UTF-8'))
				{
					return $option['label'];
				}
			}
			
		}
		}
		return "";

	}
	public function getMyUrl($product)
	{
	   $_categories = $product->getCategoryIds();
       $_category = Mage::getModel('catalog/category')->load($_categories[0]);
       $urlPath=$this->getUrl($_category->getUrlPath ());
	   $urlPath=substr($urlPath,0,strrpos($urlPath,'.')).'/'; 
	   $url =$urlPath.basename($product->getProductUrl());
	   return $url;
	}
	
}


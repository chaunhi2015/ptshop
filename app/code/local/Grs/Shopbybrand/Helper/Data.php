<?php

class Grs_Shopbybrand_Helper_Data extends Mage_Core_Helper_Abstract
{
public function getAttributes($attribute)
	{
	$attributes = Mage::getModel('catalogsearch/advanced')->getAttributes();
	$attributeArray=array();
	foreach($attributes as $a){
	if($a->getAttributeCode() == $attribute)
	{
		foreach($a->getSource()->getAllOptions(false) as $option){
			$attributeArray[$option['value']] = $option['label'];
			}
	}	
	}
	return $attributeArray;
	}
public function getAttributeLabel($attributeValue,$attributeName)
	{
		$attributes = Mage::getModel('catalogsearch/advanced')->getAttributes();
		//echo count($attributes);
		$attributeArray=array();
		foreach($attributes as $a){
		if($a->getAttributeCode() == $attributeName)
		{
			foreach($a->getSource()->getAllOptions(false) as $option){
			
			if ( mb_strtolower($option['value'],'UTF-8')==mb_strtolower($attributeValue,'UTF-8'))
				{
					return $option['label'];
				}
			}
			
		}
		}
		return "";

	}
}
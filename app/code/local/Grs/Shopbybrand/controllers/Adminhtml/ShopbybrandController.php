<?php

class Grs_Shopbybrand_Adminhtml_ShopbybrandController extends Mage_Adminhtml_Controller_action
{

	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('shopbybrand/items')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
		
		return $this;
	}   
 
	public function indexAction() {
		$this->_initAction()
			->renderLayout();
	}

	public function editAction() {
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('shopbybrand/shopbybrand')->load($id);
		
		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				
				$model->setData($data);
			}

			Mage::register('shopbybrand_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('shopbybrand/items');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item Manager'), Mage::helper('adminhtml')->__('Item Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Item News'), Mage::helper('adminhtml')->__('Item News'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('shopbybrand/adminhtml_shopbybrand_edit'))
				->_addLeft($this->getLayout()->createBlock('shopbybrand/adminhtml_shopbybrand_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('shopbybrand')->__('Item does not exist'));
			$this->_redirect('*/*/');
		}
	}
 
	public function newAction() {
		$this->_forward('edit');
	}
	public function debug($txt)
		{
			 
			$h = fopen('c:\log.txt', 'w'); //open a file
			fwrite($h, $txt);
			fclose($h); //close file
		}
	public function saveAction() {
		if ($data = $this->getRequest()->getPost()) {
			
			if(isset($_FILES['filename']['name']) && $_FILES['filename']['name'] != '') {
				try {	
					/* Starting upload */	
					$uploader = new Varien_File_Uploader('filename');
					
					// Any extention would work
	           		$uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
					$uploader->setAllowRenameFiles(false);
					
					
					// Set the file upload mode 
					// false -> get the file directly in the specified folder
					// true -> get the file in the product like folders 
					//	(file.jpg will go in something like /media/f/i/file.jpg)
					$uploader->setFilesDispersion(false);
							
					// We set media as the upload dir
					//$path = Mage::getBaseDir('media') . DS ;
					$path = Mage::getBaseDir('media')."/Brands". DS ."images". DS ;
					$data['filename'] = $_FILES['filename']['name'];				
					//Save Image Tag in DB for GRID View
					$imgName = $_FILES['filename']['name'];
					$imgPath = Mage::getBaseUrl('media')."Brands/images/thumb/".$imgName;
					$data['thumb'] = '<img src="'.$imgPath.'" border="0" width="75" height="75" />';
					$data['brandname'] = '<img src="'.$imgPath.'" border="0" width="75" height="75" />';
					//$data['brandname'] ='AAAAAAAAAAAAA';//$data['content']['text'];// Mage::helper('shopbybrand')->getAttributeLabel($data['content'],'manufacturer');
					
					$uploader->save($path, $_FILES['filename']['name'] );
					//
					
					//Create Thumbnail and upload
						$imgName = $_FILES['filename']['name'];
						$imgPathFull = $path.$imgName;
						$resizeFolder = "thumb";
						$imageResizedPath = $path.$resizeFolder.DS.$imgName;
						$imageObj = new Varien_Image($imgPathFull);
						$imageObj->constrainOnly(TRUE);
						$imageObj->keepAspectRatio(TRUE);
						$imageObj->resize(150,150);
						$imageObj->save($imageResizedPath);
					//
					
					
					
				} catch (Exception $e) {
		      
		        }
	        
		        //this way the name is saved in DB
	  			$data['filename'] = $_FILES['filename']['name'];
			}
	  			
	  		$data['brandname']=Mage::helper('shopbybrand')->getAttributeLabel($data['title'],'manufacturer');//$data['title'];	
			$model = Mage::getModel('shopbybrand/shopbybrand');		
			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));
			
			try {
				
				if ($model->getCreatedTime == NULL || $model->getUpdateTime() == NULL) {
					$model->setCreatedTime(now())
						->setUpdateTime(now());
				} else {
					$model->setUpdateTime(now());
				}	
				//$model->setId($data['title']);
				$model->save();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('shopbybrand')->__('Item was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }
        }
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('shopbybrand')->__('Unable to find item to save'));
        $this->_redirect('*/*/');
	}
 
	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$model = Mage::getModel('shopbybrand/shopbybrand');
				 
				$model->setId($this->getRequest()->getParam('id'))
					->delete();
					 
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Item was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

    public function massDeleteAction() {
        $shopbybrandIds = $this->getRequest()->getParam('shopbybrand');
        if(!is_array($shopbybrandIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
        } else {
            try {
                foreach ($shopbybrandIds as $shopbybrandId) {
                    $shopbybrand = Mage::getModel('shopbybrand/shopbybrand')->load($shopbybrandId);
                    $shopbybrand->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($shopbybrandIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
	
    public function massStatusAction()
    {
        $shopbybrandIds = $this->getRequest()->getParam('shopbybrand');
        if(!is_array($shopbybrandIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select item(s)'));
        } else {
            try {
                foreach ($shopbybrandIds as $shopbybrandId) {
                    $shopbybrand = Mage::getSingleton('shopbybrand/shopbybrand')
                        ->load($shopbybrandId)
                        ->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($shopbybrandIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/index');
    }
  
    public function exportCsvAction()
    {
        $fileName   = 'shopbybrand.csv';
        $content    = $this->getLayout()->createBlock('shopbybrand/adminhtml_shopbybrand_grid')
            ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName   = 'shopbybrand.xml';
        $content    = $this->getLayout()->createBlock('shopbybrand/adminhtml_shopbybrand_grid')
            ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
}
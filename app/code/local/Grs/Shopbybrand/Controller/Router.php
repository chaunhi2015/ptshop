<?php

class Grs_Shopbybrand_Controller_Router extends Mage_Core_Controller_Varien_Router_Abstract
{
protected $_route='brand';
public function initControllerRouters($observer)
    {
       
		$front = $observer->getEvent()->getFront();
        $filter = new Grs_Shopbybrand_Controller_Router();
        $front->addRouter($this->_route, $filter);
	
    }
public function debug($txt)
{
	 
	$h = fopen('c:\log.txt', 'w'); //open a file
	fwrite($h, $txt);
	fclose($h); //close file
}
public function match(Zend_Controller_Request_Http $request)
    {
		$_route='brand';	 
		
		if (!Mage::app()->isInstalled()) 
		{
			
			Mage::app()->getFrontController()->getResponse()
                ->setRedirect(Mage::getUrl('install'))
                ->sendResponse();
            exit;
        }
		
		
		$identifier =strtolower ($request->getPathInfo());
		
		
		
		if (substr(str_replace("/", "",$identifier), 0, strlen($_route)) != $_route)
		{
			return false;
		}

		$identifier = substr_replace($request->getPathInfo(),'', 0, strlen("/" . $_route. "/") );
		$identifier = str_replace('.html', '', $identifier);
		$identifier = str_replace('.htm', '', $identifier);
		$identifier = str_replace('.php', '', $identifier);
//
		
		//
		if ($identifier[strlen($identifier)-1] == "/")
		{
			$identifier = substr($identifier,'',-1);
		}

		//$identifier = explode('/', $identifier, 3);
		$identifier = explode('/', $identifier, 1);
		
		if (isset($identifier[0])){
			
			$request->setModuleName('shopbybrand')
				->setControllerName('index')
				->setActionName('view')
				->setParam('id', $identifier[0]);
				//->setParam('v', $identifier[1]);
			$request->setAlias(Mage_Core_Model_Url_Rewrite::REWRITE_REQUEST_PATH_ALIAS,$_route.'/'.$identifier[0].'.html');
			return true;
		} else {
			return false;
		}



    
    }
}

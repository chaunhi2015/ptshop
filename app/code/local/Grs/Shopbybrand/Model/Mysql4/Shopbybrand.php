<?php

class Grs_Shopbybrand_Model_Mysql4_Shopbybrand extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the shopbybrand_id refers to the key field in your database table.
        $this->_init('shopbybrand/shopbybrand', 'shopbybrand_id');
    }
}